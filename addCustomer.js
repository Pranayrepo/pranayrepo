(function() {
	'use strict';

	angular.module('holis.customers')
		.controller('CustomerAddCtrl', CustomerAddCtrl);

	CustomerAddCtrl.$inject = ['$scope', '$log', '$state', '$stateParams', '$q', '$modal', 'customerSvc', 'datasetSvc', 'geographySvc', 'securityRoleSvc', 'toaster'];

	function CustomerAddCtrl($scope, $log, $state, $stateParams, $q, $modal, customerSvc, datasetSvc, geographySvc, securityRoleSvc, toaster) {

		$scope.meta.title = 'Add Customer';
		$scope.meta.subtitle = '';
		$scope.formSubmitted = false;
		$scope.input = {
			poRequired: 'false',
			taxExempt: 'false'
		};
		$scope.customer = {};
		$scope.datasets = {};

		$scope.ui = {
			loading: false
		};

		$scope.save = save;
		$scope.addAddress = addAddress;
		$scope.editAddress = editAddress;
		$scope.deleteAddress = deleteAddress;
		$scope.addContact = addContact;
		$scope.editContact = editContact;
		$scope.deleteContact = deleteContact;
		init();

		function init(){

			$log.debug('CustomerAddCtrl.init() :: executed' );

			$q.all([

				datasetSvc.load('customer.type')
					.then(
						function( result ){
							angular.forEach( result.data.data, function(type){
								type.display = type.value + ' (' + type.shortCode + ')';
							});
							$scope.datasets.types = result.data.data;
						},
						function( error ){

						}
					),

				datasetSvc.load('customer.currency')
					.then(
						function( result ){
							$scope.datasets.currencies = result.data.data;
						},
						function( error ){

						}
					),

				datasetSvc.load('customer.paymentTerms')
					.then(
						function( result ){
							$scope.datasets.paymentTerms = result.data.data;
						},
						function( error ){

						}
					),

				datasetSvc.load('customer.shippingMethod')
					.then(
						function( result ){
							$scope.datasets.shippingMethods = result.data.data;
						},
						function( error ){

						}
					),

				datasetSvc.load('system.phoneType')
					.then(
						function( result ){
							$scope.datasets.phoneTypes = result.data.data;
						},
						function( error ){

						}
					),

				geographySvc.getCountries()
					.then(
						function( result ){
							$scope.datasets.countries = result.data.data;
						},
						function( error ){

						}
					),

				securityRoleSvc.getRoleFromCode('salesRep')
					.then(
					    function( result ){

							if( result.data.data.length ){

								securityRoleSvc.listUsersInRole( result.data.data[0].id )
									.then(
									    function( result ){
											$scope.datasets.salesReps = result.data.data;
											return true;
									    },
									    function( error ){

											if( error.status === 404 ){
												$scope.datasets.salesReps = [];
											} else {
												return false;
											}

									    }
									);

							} else {
								return false;
							}

					    },
					    function( error ){

					    }
					)

			])
				.then(
					function( values ){

				    }
				);
		}

		function save( isValid, redirect ){

			isValid = isValid || false;
			redirect = redirect || false;

			$scope.formSubmitted = true;

			//console.log($scope.input);

			if( isValid ){

				$scope.ui.customerContactFormSubmit = true;

				var contact = {
					accountStatus: 'active',
					name: $scope.input.name,
					type: $scope.input.type.shortCode,
					phone: $scope.input.phone,
					phoneExt: $scope.input.phoneExt,
					secondaryPhone: $scope.input.secondaryPhone,
					secondaryPhoneExt: $scope.input.secondaryPhoneExt,
					website: $scope.input.website,
					fax: $scope.input.fax,
					paymentTerms: $scope.input.paymentTerms.shortCode,
					currency: $scope.input.currency.shortCode,
					notes: $scope.input.notes,
					poRequired: $scope.input.poRequired,
					status: 'active',
                    taxExempt: $scope.input.taxExempt === 'true' ? true : false,
                    taxExemptNumber: $scope.input.taxExemptNumber
				};

				if( $scope.customer.id ){
					contact.id = $scope.customer.id;
				}

				if($scope.input.shippingMethod && $scope.input.shippingMethod.shortCode) {
                    contact.shippingMethod = $scope.input.shippingMethod.shortCode;

                }

				if( $scope.input.phoneType ){
					contact.phoneType = $scope.input.phoneType.shortCode;
				}

				if( $scope.input.secondaryPhoneType ){
					contact.secondaryPhoneType = $scope.input.secondaryPhoneType.shortCode;
				}

				if( $scope.input.salesRep ){
					contact.salesRep = $scope.input.salesRep.id;
				}

				if( $scope.input.website && $scope.input.website.length && $scope.input.website.substr(0,4) !== 'http' ){
					contact.website = 'http://' + $scope.input.website;
				}

				customerSvc.save( contact )
					.then(
						function( result ){
							$scope.customer = result.data;
							toaster.pop( 'success', 'Success', 'Customer record created successfully.' );

							if( redirect ){
								$state.go( 'root.customers.list' );
							}
						},
						function( result ){
							$scope.ui.customerContactFormSubmit = false;
							toaster.pop( 'error', 'Error', 'An error occurred while creating the customer record.' );
						}
					);

			}

		}

		function listAddresses(){

			$scope.ui.addressesLoading = true;

			customerSvc.listAddresses( $scope.customer.id, {status: 'active'} )
				.then(
					function(result){
						$scope.addresses = result.data.data;
						$scope.ui.addressesLoading = false;
						//$log.debug('CustomerEditCtrl.init() :: $scope.addresses', $scope.addresses );
					},
					function(result){
						$scope.ui.addressesLoading = false;
						if(result.status !== 404){
						toaster.pop('error','Error','An error occurred while loading the customer address records.');
						}
					}
				);

		}

		function deleteAddress(address){
			var modalInstance = $modal.open({
				templateUrl: '/app/modules/shared/partials/modal.html',
				controller: 'ModalDeleteCustomerAddressCtrl',
				backdrop: true,
				resolve: {
					title: function(){ return 'Delete Customer Address?'; },
					htmlUrl: function(){ return '/app/modules/customers/partials/modal-delete-customer-address.html'; },
					customer: function(){ return $scope.customer; },
					address: function(){ return address; }
				}
			});

			modalInstance.result.then(
				function() {
					//refresh the address list
					listAddresses();

				}, function (returnValue) {
					//dismissed -- do nothing
				});
		}

		function addAddress(){
			var modalInstance = $modal.open({
				templateUrl: '/app/modules/shared/partials/modal.html',
				controller: 'ModalAddCustomerAddressCtrl',
				backdrop: true,
				resolve: {
					title: function(){ return 'Add Customer Address'; },
					htmlUrl: function(){ return '/app/modules/customers/partials/modal-customer-address-info-form.html'; },
					customer: function(){ return $scope.customer; }
				}
			});

			modalInstance.result.then(
				function() {
					//refresh the address list
					listAddresses();

				}, function (returnValue) {
					//dismissed -- do nothing
				});
		}

		function editAddress(address){
			var modalInstance = $modal.open({
				templateUrl: '/app/modules/shared/partials/modal.html',
				controller: 'ModalEditCustomerAddressCtrl',
				backdrop: true,
				resolve: {
					title: function(){ return 'Edit Customer Address'; },
					htmlUrl: function(){ return '/app/modules/customers/partials/modal-customer-address-info-form.html'; },
					customer: function(){ return $scope.customer; },
					address: function(){ return address; }
				}
			});

			modalInstance.result.then(
				function() {
					//refresh the address list
					listAddresses();

				}, function (returnValue) {
					//dismissed -- do nothing
				});
		}

		function addContact(){
			var modalInstance = $modal.open({
				templateUrl: '/app/modules/shared/partials/modal.html',
				controller: 'ModalAddCustomerContactCtrl',
				backdrop: true,
				resolve: {
					title: function(){ return 'Add Contact'; },
					htmlUrl: function(){ return '/app/modules/customers/partials/modal-customer-contact-info-form.html'; },
					customer: function(){ return $scope.customer; }
				}
			});

			modalInstance.result.then(
				function() {
					//refresh the address list
					listContacts();

				}, function (returnValue) {
					//dismissed -- do nothing
				});
		}

		function editContact(contact){
			var modalInstance = $modal.open({
				templateUrl: '/app/modules/shared/partials/modal.html',
				controller: 'ModalEditCustomerContactCtrl',
				backdrop: true,
				resolve: {
					title: function(){ return 'Edit Customer Address'; },
					htmlUrl: function(){ return '/app/modules/customers/partials/modal-customer-contact-info-form.html'; },
					customer: function(){ return $scope.customer; },
					contact: function(){ return contact; }
				}
			});

			modalInstance.result.then(
				function() {
					//refresh the address list
					listContacts();

				}, function (returnValue) {
					//dismissed -- do nothing
				});
		}

		function deleteContact( contact ){

			var modalInstance = $modal.open({
				templateUrl: '/app/modules/shared/partials/modal.html',
				controller: 'ModalDeleteCustomerContactCtrl',
				backdrop: true,
				resolve: {
					title: function(){ return 'Delete Contact?'; },
					htmlUrl: function(){ return '/app/modules/customers/partials/modal-delete-customer-contact.html'; },
					customer: function(){ return $scope.customer; },
					contact: function(){ return contact; }
				}
			});

			modalInstance.result.then(
				function() {
					//refresh the contact list
					listContacts();

				}, function (returnValue) {
					//dismissed -- do nothing
				});

		}

		function listContacts(){

			$scope.ui.contactsLoading = true;
			$scope.contacts = [];

			customerSvc.listContacts( $scope.customer.id )
				.then(
				function( result ){
					$scope.contacts = result.data.data;
					$scope.ui.contactsLoading = false;
					return true;
				},
				function( result ){
					$scope.ui.contactsLoading = false;
					if( result.status !== 404 ){
						toaster.pop('error', 'Error', 'An error occurred loading the selected customer\'s contacts.' );
					}
					return false;
				}
			);
		}

	}


})();