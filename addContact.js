'use strict';

angular.module('holis')
    .config(function ($stateProvider) {

        $stateProvider

            .state('root.orders', {
                url: '',
                abstract: true,
                views: {
                },
                onEnter: ['$rootScope', function($rootScope){
                    $rootScope.title = 'Customer Orders';
                    $rootScope.moduleClass = 'module-customer-orders';
                }]
            })

            .state('root.orders.list', {
                url: '/orders/list',
                views: {
                    'content@root': {
                        templateUrl: 'app/modules/orders/views/order-list.html',
                        controller: 'OrderListCtrl',
                        resolve: {
                            config: function() {
                                return {
                                    showFilters: true,
                                    showCustomer: true,
                                    rows: 100
                                };
                            }
                        }

                    },
                    'orders@root.orders.list': {templateUrl: 'app/modules/orders/partials/order-list.html'}

                }
            })

            .state('root.orders.picklist', {
                url: '/orders/picklist',
                views: {
                    'content@root': {
                        templateUrl: 'app/modules/orders/views/order-pick-list.html',
                        controller: 'OrderPickListCtrl',
                        resolve: {
                            config: function () {
                                return {
                                    showFilters: true,
                                    showCustomer: true,
                                    rows: 25
                                };
                            }
                        }

                    },
                    'orders@root.orders.picklist': {templateUrl: 'app/modules/orders/partials/order-pick-list.html'}
                }
            })
            .state('root.orders.pick', {
                url: '/orders/:orderId/pick',
                views: {
                    'content@root': {
                        templateUrl: 'app/modules/orders/views/order-pick-list.html',
                        controller: 'OrderPickListCtrl',
                        resolve: {
                            config: function () {
                                return {
                                    showFilters: true,
                                    showCustomer: true,
                                    rows: 25
                                };
                            }
                        }

                    },
                    'orders@root.orders.picklist': {templateUrl: 'app/modules/orders/partials/order-pick-list.html'}
                }
            })

            .state('root.orders.pick.modify', {
                url: '/modify',
                views: {
                    'content@root': {
                        templateUrl: 'app/modules/orders/views/order-modify-location.html',
                        //controller: 'OrderListCtrl'
                    }
                }
            })

            .state('root.orders.pick.changeplant', {
                url: '/changeplant',
                views: {
                    'content@root': {
                        templateUrl: 'app/modules/orders/views/order-change-plant.html',
                        //controller: 'OrderListCtrl'
                    }
                }
            })

            .state('root.orders.new', {
                url: '/orders/new',
                views: {
                    'content@root': {templateUrl: 'app/modules/orders/views/select-customer.html', controller: 'OrderSelectCustomerCtrl'}
                }
            })

            .state('root.orders.add', {
                url: '/orders/add/:customerId',
                views: {
                    'content@root': {templateUrl: 'app/modules/orders/views/order-form.html', controller: 'OrderAddCtrl'},
                    'orderForm@root.orders.add': {templateUrl: 'app/modules/orders/partials/info-form.html'},
                    'skuList@root.orders.add': {templateUrl: 'app/modules/orders/partials/product-search.html', controller: 'OrderAddProductSearchCtrl'},
                    'currentItems@root.orders.add': {templateUrl: 'app/modules/orders/partials/order-items.html', controller: 'OrderItemsCtrl'}
              }
            })

            .state('root.orders.edit', {
                url: '/orders/:orderId/edit',
                params: {isDuplicate: false},
                views: {
                    'content@root': {templateUrl: 'app/modules/orders/views/order-form.html', controller: 'OrderEditCtrl'},
                    'orderForm@root.orders.edit': {templateUrl: 'app/modules/orders/partials/info-form.html'},
                    'skuList@root.orders.edit': {templateUrl: 'app/modules/orders/partials/product-search.html', controller: 'OrderAddProductSearchCtrl'},
                    'currentItems@root.orders.edit': {templateUrl: 'app/modules/orders/partials/order-items.html', controller: 'OrderItemsCtrl'},
                    'history@root.orders.edit': {templateUrl: 'app/modules/orders/partials/order-history.html', controller: 'OrderHistoryCtrl'}
                }
            })
            .state('root.orders.picklistedit', {
                url: '/orders/:orderId/picklistedit',
                views: {
                    'content@root': {templateUrl: 'app/modules/orders/views/order-pick-list-form.html', controller: 'OrderEditCtrl'},
                    'currentItems@root.orders.picklistedit': {templateUrl: 'app/modules/orders/partials/order-items.html', controller: 'OrderItemsCtrl'}
                }
            })

            .state('root.orders.view', {
                url: '/orders/:orderId',
                views: {
                    'content@root': {templateUrl: 'app/modules/orders/views/order.html', controller: 'OrderViewCtrl'},
                    'order@root.orders.view': {templateUrl: 'app/modules/orders/partials/order-detail.html'}
                    //'history@root.orders.view': {templateUrl: 'app/modules/orders/partials/order-history.html', controller: 'OrderHistoryCtrl'}
                }
            })
            .state('root.orders.invoice', {
                url: '/orders/:orderId/invoice',
                views: {
                    'content@root': {templateUrl: 'app/modules/orders/views/order.html', controller: 'OrderInvoiceCtrl'},
                    'order@root.orders.invoice': {templateUrl: 'app/modules/orders/partials/order-invoice-detail.html'},
                }
            })
            .state('root.orders.invoices', {
                url: '/invoices',
                views: {
                    'content@root': {templateUrl: 'app/modules/orders/views/order-invoices.html', controller: 'OrderInvoiceListCtrl'}
                    }
            })



        ;



    });
