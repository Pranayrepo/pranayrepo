(function() {
	'use strict';

	/* Controllers */

	angular.module('holis.customers')
		.controller('CustomersListCtrl', CustomersListCtrl);

	CustomersListCtrl.$inject = ['$scope', '$log', '$state', '$modal', '$q', 'toaster', 'datasetSvc', 'customerSvc', 'moment'];

	function CustomersListCtrl($scope, $log, $state, $modal, $q, toaster, datasetSvc, customerSvc, moment) {

		$scope.meta.title = 'Customer List';
		$scope.meta.subtitle = '';

		$scope.ui = {
			loading: false,
			filterPanel: {
				show: true
			},
			datePicker: {
				lastOrderDate: false
			}

		};

		$scope.criteria = {
			type: null,
			currentPage: 1,
			rows: 100,
			sortColumn: 'name',
			sortDirection: 'ASC',
			status: 'active',
			isActive: true
		};

		$scope.datasets = {};

		$scope.$watch('criteria.currentPage', criteriaChangeHandler, true);

		$scope.listCustomers = listCustomers;
		$scope.sortByColumn = sortByColumn;
		$scope.quickSearch = quickSearch;
		$scope.quickSearchSelect = quickSearchSelect;
		$scope.clear = resetCriteria;
		$scope.delete = deleteCustomer;
		$scope.openDatePicker = openDatePicker;

		init();

		function init() {

			$scope.ui.loading = true;

			$q.all([

				datasetSvc.load('customer.status')
					.then(
						function (result) {

							$scope.datasets.status = result.data;

							return true;
						},
						function (result) {
							return false;
						}
					),

				datasetSvc.load('customer.type')
					.then(
						function (result) {

							$scope.datasets.types = result.data;

							return true;
						},
						function (result) {
							return false;
						}
					)


			])
				.then(function (values) {

					var success = true;

					for (var v = 0; v < values.length; v++) {

						if (values[v] === false) {
							success = false;
							break;
						}

					}

					if (success) {

						angular.forEach($scope.datasets.status.data, function( status ){
							if( status.shortCode === 'active' ){
								$scope.criteria.status = status;
							}
						});

						listCustomers();

					} else {
						toaster.pop('error', 'Error', 'An error occurred while ');
					}

				});

		}

		function listCustomers( criteria ){

			$scope.ui.loading = true;

			criteria = criteria || angular.copy($scope.criteria);

			criteria.start = ((criteria.currentPage - 1) * criteria.rows ) + 1;

			if( criteria.sortColumn.length ){
				criteria.orderBy = criteria.sortColumn + ' ' + criteria.sortDirection;
			}

			if( criteria.status.id ){
				criteria.status = criteria.status.shortCode;
			}

			if( criteria.type && criteria.type.id ){
				criteria.type = criteria.type.shortCode;
			} else {
				delete criteria.type;
			}

			if( criteria.name && criteria.name.length===0 ){
				delete criteria.name;
			}

			if( criteria.arBalance && criteria.arBalanceRange ){
				if( criteria.arBalanceRange === 'lessThan'){
					criteria.arBalance = '0:' + criteria.arBalance.toString();
				} else if( criteria.arBalanceRange === 'greaterThan'){
					criteria.arBalance = criteria.arBalance.toString() + ':9999999';
				} else {
					delete criteria.arBalance;
				}
				
				delete criteria.arBalanceRange;
			} else {
				delete criteria.arBalance;
				delete criteria.arBalanceRange;
			}

			if( criteria.lastOrderDate && criteria.lastOrderDateRange ){

				if( criteria.lastOrderDateRange === 'before'){
					criteria.lastOrderDate = moment('1970-01-01').toISOString() + ':' + moment(criteria.lastOrderDate).endOf('day').toISOString();
				} else if( criteria.lastOrderDateRange === 'after'){
					criteria.lastOrderDate = moment(criteria.lastOrderDate).endOf('day').toISOString() + ':' + moment('2050-12-31').endOf('day').toISOString();
				} else {
					delete criteria.lastOrderDate;
				}
				delete criteria.lastOrderDateRange;

			} else {
				delete criteria.lastOrderDate;
				delete criteria.lastOrderDateRange;
			}

			delete criteria.sortColumn;
			delete criteria.sortDirection;

			customerSvc.list( criteria )
				.then(
					function( result ){
						$scope.customers = result.data;
						updateVisibleRowStats();
						$scope.ui.loading = false;
						return true;
					},
					function( result ){
						$scope.ui.loading = false;

						if( result.status !== 404 ){
							toaster.pop('error', 'Error', 'An error occurred while loading the list of customers.' );
							return false;
						} else {
							$scope.customers = {start: 0, end: 0, numFound: 0, data: []};
							return true;
						}

					}
				);
		}

		function updateVisibleRowStats(){
			var end = ($scope.customers.start + $scope.criteria.rows) - 1;

			if( end > $scope.customers.numFound ){
				end = $scope.customers.numFound;
			}
			$scope.customers.end = end;
		}

		function sortByColumn(column){

			if( $scope.criteria.sortColumn.toLowerCase() !== column.toLowerCase() ){
				$scope.criteria.sortColumn = column;
				$scope.criteria.sortDirection = 'ASC';
			} else if( $scope.criteria.sortDirection === 'ASC' ){
				$scope.criteria.sortDirection = 'DESC';
			} else if( $scope.criteria.sortDirection === 'DESC' ){
				$scope.criteria.sortDirection = 'ASC';
				$scope.criteria.sortColumn = '';
			}
			listCustomers();
		}

		function quickSearch(name){

			var criteria = {name: name, rows: 5, orderBy: 'name ASC'};

			return customerSvc.list( criteria )
				.then(
					function( result ){

						if( result.data.numFound > result.data.rows ){
							result.data.data.unshift( {id: 'all', name: 'Show all ' + result.data.numFound + ' customers matching: ' + name, query: name } );
						}

						return result.data.data;

					},
					function( result ){

						if( result.status !== 404 ){
							toaster.pop('error', 'Error', 'An error occurred while searching customers.' );
							return [];
						} else {
							return [];
						}

					}
				);

		}

		function quickSearchSelect(item, model, label){

			var criteria = angular.copy( $scope.criteria );
			criteria.name = item.name;
			criteria.currentPage = 1;

			$scope.criteria = criteria;

			listCustomers();

		}

		function deleteCustomer(customer){
			var modalInstance = $modal.open({
				templateUrl: '/app/modules/shared/partials/modal.html',
				controller: 'ModalDeleteCustomerCtrl',
				backdrop: true,
				resolve: {
					title: function(){ return 'Delete Customer?'; },
					htmlUrl: function(){ return '/app/modules/customers/partials/modal-delete-customer.html'; },
					customer: function(){ return customer; }
				}
			});

			modalInstance.result.then(
					function() {
						//refresh the suppliers list
						$scope.listCustomers();

					}, function (returnValue) {
						//dismissed -- do nothing
					});
		}

		function resetCriteria(){
			var criteria = {
				type: null,
				currentPage: 1,
				rows: 100,
				sortColumn: 'name',
				sortDirection: 'ASC',
				status: 'active',
				lastOrderDate: null,
				lastOrderDateRange: '',
				arBalance: '',
				arBalanceRange: '',
				isActive: true
			};

			angular.forEach($scope.datasets.status.data, function( status ){
				if( status.shortCode === 'active' ){
					criteria.status = status;
				}
			});

			$scope.quickSearchInput = '';

			$scope.criteria = criteria;

			listCustomers();

		}

		function openDatePicker($event, name){
			$event.preventDefault();
			$event.stopPropagation();

			angular.forEach( $scope.ui.datePicker, function(picker){
				picker = false;
			});

			$scope.ui.datePicker[ name ] = true;

		}

		function criteriaChangeHandler(newValue, oldValue) {
			if (newValue && newValue !== oldValue) {
				listCustomers();
			}
		}
	}
})();