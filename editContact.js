(function() {
    'use strict';

    angular.module('holis.customers')
        .controller('CustomerEditContactCtrl', CustomerEditContactCtrl);

    CustomerEditContactCtrl.$inject = ['$scope', '$state', '$stateParams', '$q', 'customerSvc', 'datasetSvc', 'toaster'];

    function CustomerEditContactCtrl($scope, $state, $stateParams, $q, customerSvc, datasetSvc, toaster) {

        $scope.meta.title = 'Add Contact';
        $scope.meta.subtitle = '';
        $scope.customer = {};
        $scope.contacts = [];
        $scope.formSubmitted = false;
        $scope.input = {};
        $scope.datasets = {};
        $scope.ui = {
            loading: true
        };

		$scope.save = save;

		init();

		function init() {

			$q.all([

				customerSvc.load($stateParams.customerId)
					.then(
						function (result) {
							$scope.customer = result.data;
							$scope.meta.subtitle = result.data.name;
							return true;
						},
						function (result) {

							if (result.status === 404) {
								$state.go('root.notfound');
							} else {
								toaster.pop('error', 'Error', 'An error occurred while loading the customer record.');
							}

							return false;
						}
					),


				customerSvc.loadContact($stateParams.customerId, $stateParams.contactId)
					.then(
						function (result) {
							$scope.input.firstName = result.data.firstName;
							$scope.input.lastName = result.data.lastName;
							$scope.input.email = result.data.email;
							$scope.input.title = result.data.title;
							$scope.input.notes = result.data.notes;
							$scope.input.phone = result.data.phone;
							$scope.input.phoneExt = result.data.phoneExt;
							$scope.input.secondaryPhone = result.data.secondaryPhone;
							$scope.input.secondaryPhoneExt = result.data.secondaryPhoneExt;
							$scope.input.status = result.data.status;
							$scope.input.isPrimary = result.data.isPrimary;
							$scope.input.status = result.data.status.shortCode;

							return true;
						},
						function (error) {
							if (result.status === 404) {
								$state.go('root.notfound');
							} else {
								toaster.pop('error', 'Error', 'An error occurred while loading the customer contact record.');
							}

							return false;
						}
					)

			])
			.then(function (values) {

				var success = true;

				for (var v = 0; v < values.length; v++) {

					if (values[v] === false) {
						success = false;
						break;
					}

				}

				if (success) {

					$scope.ui.loading = false;
				} else {

					toaster.pop('error', 'Error', 'An error occurred loading the selected customer contact info.');
				}

			});
		}

        function save( isValid ){

            $scope.formSubmitted = true;

            if( isValid ){

                $scope.ui.customerContactFormSubmit = true;

                var contact = {

                    customerId: $stateParams.customerId,
                    contactId: $stateParams.contactId,
                    firstName: $scope.input.firstName,
                    lastName: $scope.input.lastName,
                    email: $scope.input.email,
                    title: $scope.input.title,
                    notes: $scope.input.notes,
                    phone: $scope.input.phone,
                    phoneExt: $scope.input.phoneExt,
                    secondaryPhone: $scope.input.secondaryPhone,
                    secondaryPhoneExt: $scope.input.secondaryPhoneExt,
					status: $scope.input.status
                };

                if( $scope.input.isPrimary === true ){
                    contact.isPrimary = true;
                    contact.isSecondary = false;
                } else {
                    contact.isPrimary = false;
                    contact.isSecondary = true;
                }

                customerSvc.updateContact( contact )
                    .then(
                    	function( result ){
                    		toaster.pop( 'success', 'Success', 'Contact information updated successfully.' );
                            $state.go( 'root.customers.contacts', {customerId: $stateParams.customerId} );
                    	},
                    	function( result ){
                            $scope.ui.customerContactFormSubmit = false;
                    		toaster.pop( 'error', 'Error', 'An error occurred while updating the contact.' );
                    	}
                    );

            }

        }

    }
})();

