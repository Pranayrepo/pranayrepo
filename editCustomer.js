(function() {
	'use strict';

	angular.module('holis.customers')
		.controller('CustomerEditCtrl', CustomerEditCtrl);

	CustomerEditCtrl.$inject = ['$scope', '$log', '$state', '$stateParams', '$q', '$modal', 'customerSvc', 'datasetSvc', 'geographySvc', 'securityRoleSvc', 'toaster'];

	function CustomerEditCtrl($scope, $log, $state, $stateParams, $q, $modal, customerSvc, datasetSvc, geographySvc, securityRoleSvc, toaster) {

		$scope.meta.title = 'Edit Customer';
		$scope.meta.subtitle = '';
		$scope.formSubmitted = false;
		$scope.input = {
			poRequired: 'false'
		};
		$scope.customer = {};
		$scope.addresses = [];
		$scope.contacts = [];
		$scope.datasets = {};

		$scope.ui = {
			loading: true,
			addressesLoading: true,
			contactsLoading: true
		};

		$scope.save = save;
		$scope.addAddress = addAddress;
		$scope.editAddress = editAddress;
		$scope.deleteAddress = deleteAddress;
		$scope.addContact = addContact;
		$scope.editContact = editContact;
		$scope.deleteContact = deleteContact;


		init();

		function init(){

			$log.debug('CustomerEditCtrl.init() :: executed' );

			$scope.ui.loading = true;
			$q.all([

				customerSvc.load( $stateParams.customerId)
					.then(
						function( result ){
							$scope.customer = result.data;
							$scope.meta.subtitle = $scope.customer.name;

							$scope.input = {
								name: $scope.customer.name,
								phone: $scope.customer.phone,
								phoneExt: $scope.customer.phoneExt,
								secondaryPhone: $scope.customer.secondaryPhone,
								secondaryPhoneExt: $scope.customer.secondaryPhoneExt,
								website: $scope.customer.website,
								fax: $scope.customer.fax,
								street1: $scope.customer.street1,
								street2: $scope.customer.street2,
								city: $scope.customer.city,
								postalCode: $scope.customer.postalCode,
								latitude: $scope.customer.latitude,
								longitude: $scope.customer.longitude,
								addressNotes: $scope.customer.addressNotes,
								notes: $scope.customer.notes,
								status: $scope.customer.status.shortCode,
								poRequired: 'false',
								taxExemptNumber: $scope.customer.taxExemptNumber,
								orderDiscount: $scope.customer.orderDiscount
							};

							if( $scope.customer.poRequired && $scope.customer.poRequired === 1 ){
								$scope.input.poRequired = 'true';
							}

                            if( $scope.customer.taxExempt && $scope.customer.taxExempt === 1 ){
                                $scope.input.taxExempt = 'true';
                            }
						},
						function( error ){

							if( error.status === 404 ){
								$state.go('root.notfound');
							} else {
								toaster.pop('error','Error','An error occurred while loading the customer record.');
							}
						}
					),

				listAddresses(),
				listContacts(),

				datasetSvc.load('customer.type')
					.then(
						function( result ){
							angular.forEach( result.data.data, function(type){
								type.display = type.value + ' (' + type.shortCode + ')';
							});
							$scope.datasets.types = result.data.data;
						},
						function( error ){
							$scope.datasets.types = [];
						}
					),

				datasetSvc.load('customer.currency')
					.then(
						function( result ){
							$scope.datasets.currencies = result.data.data;
						},
						function( error ){
							$scope.datasets.currencies = [];
						}
					),

				datasetSvc.load('customer.paymentTerms')
					.then(
						function( result ){
							$scope.datasets.paymentTerms = result.data.data;
						},
						function( error ){
							$scope.datasets.paymentTerms = [];
						}
					),

				datasetSvc.load('customer.shippingMethod')
					.then(
						function( result ){
							$scope.datasets.shippingMethods = result.data.data;
						},
						function( error ){
							$scope.datasets.shippingMethods = [];
						}
					),

				datasetSvc.load('system.phoneType')
					.then(
						function( result ){
							$scope.datasets.phoneTypes = result.data.data;
						},
						function( error ){
							$scope.datasets.phoneTypes = [];
						}
					),

				securityRoleSvc.getRoleFromCode('salesRep')
					.then(
					    function( result ){

							if( result.data.data.length ){

								return securityRoleSvc.listUsersInRole( result.data.data[0].id )
									.then(
									    function( result ){

											$scope.datasets.salesReps = result.data.data;

											return true;
									    },
									    function( error ){

											if( error.status === 404 ){
												$scope.datasets.salesReps = [];
											} else {
												return false;
											}

									    }
									);

							} else {
								return false;
							}

					    },
					    function( error ){

					    }
					)

			])
				.then(
					function( values ){

						angular.forEach($scope.datasets.types, function(item){
							if( item.shortCode === $scope.customer.type.shortCode ){
								$scope.input.type = item;
							}
						});

						angular.forEach($scope.datasets.phoneTypes, function(item){
							if( $scope.customer.phoneType && item.shortCode === $scope.customer.phoneType.shortCode ){
								$scope.input.phoneType = item;
							}
						});

						angular.forEach($scope.datasets.phoneTypes, function(item){
							if( $scope.customer.secondaryPhoneType && item.shortCode === $scope.customer.secondaryPhoneType.shortCode ){
								$scope.input.secondaryPhoneType = item;
							}
						});

						angular.forEach($scope.datasets.paymentTerms, function(item){
							if( item.shortCode === $scope.customer.paymentTerms.shortCode ){
								$scope.input.paymentTerms = item;
							}
						});

						angular.forEach($scope.datasets.currencies, function(item){
							if( item.shortCode.toLowerCase() === $scope.customer.currency.shortCode.toLowerCase() ){
								$scope.input.currency = item;
							}
						});

						angular.forEach($scope.datasets.states, function(item){
							if( item.shortCode === $scope.customer.state.shortCode ){
								$scope.input.state = item;
							}
						});

						angular.forEach($scope.datasets.shippingMethods, function(item){
							if( item.shortCode === $scope.customer.shippingMethod.shortCode ){
								$scope.input.shippingMethod = item;
							}
						});

						angular.forEach($scope.datasets.salesReps, function(user){

							if( $scope.customer.salesRep && $scope.customer.salesRep.id === user.id  ){
								$scope.input.salesRep = user;
							}
						});

						$scope.ui.loading = false;
				    }
				);
		}

		function save( isValid, redirect ){

			isValid = isValid || false;
			redirect = redirect || false;

			$scope.formSubmitted = true;

			if( isValid ){

				$scope.ui.customerContactFormSubmit = true;

				var contact = {
					accountStatus: 'active',
					name: $scope.input.name,
					type: $scope.input.type.shortCode,
					phone: $scope.input.phone,
					phoneExt: $scope.input.phoneExt,
					secondaryPhone: $scope.input.secondaryPhone,
					secondaryPhoneExt: $scope.input.secondaryPhoneExt,
					website: $scope.input.website,
					fax: $scope.input.fax,
					paymentTerms: $scope.input.paymentTerms.shortCode,
					currency: $scope.input.currency.shortCode,
					shippingMethod: $scope.input.shippingMethod.shortCode,
					notes: $scope.input.notes,
					poRequired: $scope.input.poRequired,
					status: $scope.input.status,
                    taxExempt: $scope.input.taxExempt === 'true' ? true : false,
                    taxExemptNumber: $scope.input.taxExemptNumber,
					orderDiscount: $scope.input.orderDiscount
				};

				if( $scope.customer.id ){
					contact.id = $scope.customer.id;
				}

				if( $scope.input.phoneType ){
					contact.phoneType = $scope.input.phoneType.shortCode;
				}

				if( $scope.input.secondaryPhoneType ){
					contact.secondaryPhoneType = $scope.input.secondaryPhoneType.shortCode;
				}

				if( $scope.input.salesRep ){
					contact.salesRep = $scope.input.salesRep.id;
				}

				if( contact.website && contact.website.length && contact.website.substr(0,4) !== 'http' ){
					contact.website = 'http://' + contact.website;
				}

				customerSvc.save( contact )
					.then(
						function( result ){
							$scope.customer = result.data;
							toaster.pop( 'success', 'Success', 'Customer record created successfully.' );

							if( redirect ){
								$state.go( 'root.customers.list' );
							} else {
								$scope.ui.loading = false;
							}
						},
						function( result ){
							$scope.ui.customerContactFormSubmit = false;
							toaster.pop( 'error', 'Error', 'An error occurred while creating the customer record.' );
						}
					);

			}

		}

		function listAddresses(){

			$scope.ui.addressesLoading = true;

			customerSvc.listAddresses( $stateParams.customerId, {status: 'active'} )
				.then(
					function(result){
						$scope.addresses = result.data.data;
						$scope.ui.addressesLoading = false;
						//$log.debug('CustomerEditCtrl.init() :: $scope.addresses', $scope.addresses );
					},
					function(result){
						$scope.ui.addressesLoading = false;
						if(result.status !== 404){
						toaster.pop('error','Error','An error occurred while loading the customer address records.');
						}
					}
				);

		}

		function listContacts(){

			$scope.ui.contactsLoading = true;
			$scope.contacts = [];

			customerSvc.listContacts( $stateParams.customerId )
				.then(
				function( result ){
					$scope.contacts = result.data.data;
					$scope.ui.contactsLoading = false;
					return true;
				},
				function( result ){
					$scope.ui.contactsLoading = false;
					if( result.status !== 404 ){
						toaster.pop('error', 'Error', 'An error occurred loading the selected customer\'s contacts.' );
					}
					return false;
				}
			);
		}

		function deleteAddress(address){
			var modalInstance = $modal.open({
				templateUrl: '/app/modules/shared/partials/modal.html',
				controller: 'ModalDeleteCustomerAddressCtrl',
				backdrop: true,
				resolve: {
					title: function(){ return 'Delete Customer Address?'; },
					htmlUrl: function(){ return '/app/modules/customers/partials/modal-delete-customer-address.html'; },
					customer: function(){ return $scope.customer; },
					address: function(){ return address; }
				}
			});

			modalInstance.result.then(
				function() {
					//refresh the address list
					listAddresses();

				}, function (returnValue) {
					//dismissed -- do nothing
				});
		}

		function deleteContact( contact ){

			var modalInstance = $modal.open({
				templateUrl: '/app/modules/shared/partials/modal.html',
				controller: 'ModalDeleteCustomerContactCtrl',
				backdrop: true,
				resolve: {
					title: function(){ return 'Delete Contact?'; },
					htmlUrl: function(){ return '/app/modules/customers/partials/modal-delete-customer-contact.html'; },
					customer: function(){ return $scope.customer; },
					contact: function(){ return contact; }
				}
			});

			modalInstance.result.then(
				function() {
					//refresh the contact list
					listContacts();

				}, function (returnValue) {
					//dismissed -- do nothing
				});

		}

		function addAddress(){
			var modalInstance = $modal.open({
				templateUrl: '/app/modules/shared/partials/modal.html',
				controller: 'ModalAddCustomerAddressCtrl',
				backdrop: true,
				resolve: {
					title: function(){ return 'Add Customer Address'; },
					htmlUrl: function(){ return '/app/modules/customers/partials/modal-customer-address-info-form.html'; },
					customer: function(){ return $scope.customer; }
				}
			});

			modalInstance.result.then(
				function() {
					//refresh the address list
					listAddresses();

				}, function (returnValue) {
					//dismissed -- do nothing
				});
		}

		function addContact(){
			var modalInstance = $modal.open({
				templateUrl: '/app/modules/shared/partials/modal.html',
				controller: 'ModalAddCustomerContactCtrl',
				backdrop: true,
				resolve: {
					title: function(){ return 'Add Contact'; },
					htmlUrl: function(){ return '/app/modules/customers/partials/modal-customer-contact-info-form.html'; },
					customer: function(){ return $scope.customer; }
				}
			});

			modalInstance.result.then(
				function() {
					//refresh the address list
					listContacts();

				}, function (returnValue) {
					//dismissed -- do nothing
				});
		}

		function editAddress(address){
			var modalInstance = $modal.open({
				templateUrl: '/app/modules/shared/partials/modal.html',
				controller: 'ModalEditCustomerAddressCtrl',
				backdrop: true,
				resolve: {
					title: function(){ return 'Edit Customer Address'; },
					htmlUrl: function(){ return '/app/modules/customers/partials/modal-customer-address-info-form.html'; },
					customer: function(){ return $scope.customer; },
					address: function(){ return address; }
				}
			});

			modalInstance.result.then(
				function() {
					//refresh the address list
					listAddresses();

				}, function (returnValue) {
					//dismissed -- do nothing
				});
		}
		function editContact(contact){
			var modalInstance = $modal.open({
				templateUrl: '/app/modules/shared/partials/modal.html',
				controller: 'ModalEditCustomerContactCtrl',
				backdrop: true,
				resolve: {
					title: function(){ return 'Edit Customer Address'; },
					htmlUrl: function(){ return '/app/modules/customers/partials/modal-customer-contact-info-form.html'; },
					customer: function(){ return $scope.customer; },
					contact: function(){ return contact; }
				}
			});

			modalInstance.result.then(
				function() {
					//refresh the address list
					listContacts();

				}, function (returnValue) {
					//dismissed -- do nothing
				});
		}
	}
})();